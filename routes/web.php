<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

route::get('data-tables', function () {
    return view('data');
});

Route::get('/pertanyaan/create', 'PertanyaanController@create');
Route::POST('/pertanyaan', 'PertanyaanController@store');
Route::get('/pertanyaan', 'PertanyaanController@index');
Route::get('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@show');
Route::get('/pertanyaan/{pertanyaan_id}/edit', 'PertanyaanController@edit');
Route::get('/pertanyaan/{pertanyaan_id}/update', 'PertanyaanController@update');
Route::get('/pertanyaan/{pertanyaan_id}/delete', 'PertanyaanController@destroy');

