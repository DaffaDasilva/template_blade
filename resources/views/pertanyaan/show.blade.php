@extends('template.master')

@section('content')
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Detail Pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
            
            @foreach($query as $pertanyaan)
                <div class="card-body">
                  <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" class="form-control" id="judul" name="judul" placeholder="Masukan Judul" disabled value="{{ $pertanyaan->judul }}">
                  </div>
                  <div class="form-group">
                    <label for="isi">Pertanyaan</label>
                    <input type="text" class="form-control" id="isi" name="isi" placeholder="enter isi" disabled value="{{ $pertanyaan->isi }}">
                  </div>
                  <div class="form-group">
                    <label for="isi">Created At</label>
                    <input type="text" class="form-control" id="created_at" name="created_at" placeholder="created at" disabled value="{{ $pertanyaan->created_at }}">
                  </div>
                  <div class="form-group">
                    <label for="isi">Updated At</label>
                    <input type="text" class="form-control" id="updated_at" name="updated_at" placeholder="updated at" disabled value="{{ $pertanyaan->updated_at }}">
                  </div>
                  <div class="form-group">
                    <label for="isi">ID Jawaban Tepat</label>
                    <input type="text" class="form-control" id="jawaban_tepat_id" name="jawaban_tepat_id" placeholder="id jawaban tepat" disabled value="{{ $pertanyaan->jawaban_tepat_id }}">
                  </div>
                  <div class="form-group">
                    <label for="isi">ID Profil</label>
                    <input type="text" class="form-control" id="profiles_id" name="profiles_id" placeholder="id profiles" disabled value="{{ $pertanyaan->profiles_id }}">
                  </div>
                </div>
            @endforeach    
            
                <div class="card-footer">
                  <a href="{{url('/pertanyaan')}}" class="btn btn-warning">Back</a>
                </div>
</div>
@endsection