@extends('template.master')

@section('content')
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">List Pertanyaan</h3>
              </div>
             
                <div class="card-body">
                @if ($message = Session::get('update'))
				<div class="alert alert-primary alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
					<strong>{{ $message }}</strong>
				</div>
				@endif
                @if ($message = Session::get('sukses'))
				<div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
					<strong>{{ $message }}</strong>
				</div>
		        @endif
                @if ($message = Session::get('delete'))
				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
					<strong>{{ $message }}</strong>
				</div>
		        @endif
                <div class="row p-2">
                    <a href="{{url('/pertanyaan/create')}}" class="btn btn-primary">Tambah Pertanyaan</a>
                    <a href="{{url('/pertanyaan')}}" class="btn btn-warning mx-2">Refresh</a>
                </div>
                <table id="example1" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th>Judul</th>
            <th>Isi</th>
            <th>Created At</th>
            <th>Updated At</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody>
          @foreach($query as $pertanyaan)
          <?php
            $url = '/pertanyaan/'.$pertanyaan->id;
            $urledit = '/pertanyaan/'.$pertanyaan->id.'/edit';
            $urldelete = '/pertanyaan/'.$pertanyaan->id.'/delete';
          ?>
          <tr>
            <td>{{$pertanyaan->judul}}</td>
            <td>{{$pertanyaan->isi}}</td>
            <td>{{$pertanyaan->created_at}}</td>
            <td>{{$pertanyaan->updated_at}}</td>
            <td>
                <form role="form" action="{{url($urldelete)}}" method="DELETE">
                  @csrf
                  <input type="hidden" class="form-control" id="id" name="id" placeholder="id" value="{{$pertanyaan->id}}">
                  <a class="btn btn-success" href="{{url($url)}}">Show</a>
                  <a class="btn btn-primary" href="{{url($urledit)}}">Edit</a>
                  <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
          </tr>
          @endforeach
          </tbody>
          <tfoot>
          <tr>
            <th>Judul</th>
            <th>Isi</th>
            <th>Created At</th>
            <th>Updated At</th>
            <th>Action</th>
          </tr>
          </tfoot>
        </table>
        </div>

        <div class="card-footer"></div>
</div>
@endsection