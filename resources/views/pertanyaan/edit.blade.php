@extends('template.master')

@section('content')
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Pertanyaan ID : @foreach($query as $pertanyaan){{$pertanyaan->id}}@endforeach()</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
            
            @foreach($query as $pertanyaan)
            <?php
                $url = '/pertanyaan/'.$pertanyaan->id.'/update';
            ?>
            <form role="form" action="{{url($url)}}" method="PUT">
              @csrf
                <div class="card-body">
                  <input type="hidden" class="form-control" id="id" name="id" placeholder="id" value="{{$pertanyaan->id}}">
                  <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" class="form-control" id="judul" name="judul" placeholder="Masukan Judul" value="{{ $pertanyaan->judul }}">
                    @error('judul')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="isi">Masukan Pertanyaan</label>
                    <input type="text" class="form-control" id="isi" name="isi" placeholder="enter isi" value="{{ $pertanyaan->isi }}">
                    @error('isi')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <div class="card-footer">
                  <a href="{{url('/pertanyaan')}}" class="btn btn-warning">Cancel</a>
                  <button type="submit" class="btn btn-success">Update</button>
                </div>
              </form>
            @endforeach    
</div>
@endsection