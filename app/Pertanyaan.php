<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    //
    protected $table = "pertanyaan";
    protected $fillable =
        [
            'judul',
            'isi',
            'created_at',
            'updated_at',
            'jawaban_tepat_id',
            'profiles_id'

        ];
}
