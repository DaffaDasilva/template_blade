<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\Pertanyaan;
use Session;

class PertanyaanController extends Controller
{
    public function index(){
        $query = Pertanyaan::all();
        return view('pertanyaan.index',['query' => $query]);
    }

    public function show($pertanyaan_id){
        $query = Pertanyaan::where('id',$pertanyaan_id)->get();
        return view('pertanyaan.show',['query' => $query]);
    }

    public function edit($pertanyaan_id){
        $query = Pertanyaan::where('id',$pertanyaan_id)->get();
        return view('pertanyaan.edit',['query' => $query]);
    }

    public function create(){
        return view('pertanyaan.create');
    }

    public function store(request $request){
       // dd($request->all());
       
       $request->validate([
          "judul" => 'required|unique:pertanyaan',
          "isi" => 'required'
       ]);

       try{
            $query = Pertanyaan::create([
                "judul" => $request ["judul"],
                "isi" => $request ["isi"]
            ]);
            Session::flash('sukses','Data pertanyaan sukses di input ke database.');
       } catch(QueryException $e){
            $message = $e->getMessage();
       }

       //print_r($query);

       return redirect('/pertanyaan');
    }

    public function update(request $request){
        // dd($request->all());
        
        $request->validate([
           "judul" => 'required',
           "isi" => 'required'
        ]);
 
        try{
             $query = Pertanyaan::where('id',$request->id)->update([
                 "judul" => $request ["judul"],
                 "isi" => $request ["isi"]
             ]);
             Session::flash('update','Data pertanyaan telah di update ke database.');
        } catch(QueryException $e){
             $message = $e->getMessage();
        }
 
        //print_r($query);
 
        return redirect('/pertanyaan');
     }

    public function destroy(request $request){
        //echo "ini delete ".$request->id;
        try{
            $query = Pertanyaan::where('id',$request->id)->delete();
            Session::flash('delete','Data pertanyaan dengan id : '.$request->id.' telah di hapus ke database.');
        }catch(QueryException $e){
            $message= $e->getMessage();
        }

        return redirect('/pertanyaan');
    }
}
